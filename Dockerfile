FROM node:12.8.1-alpine

ENV PATH /node_modules/.bin:$PATH

COPY src ./src
COPY public ./public
COPY package*.json ./

RUN npm install --silent
RUN npm install -g react-scripts

EXPOSE 3000
CMD ["npm", "run", "dev"]
