export interface User {
    id: string,
    name?: string,
    avatar?: string
}

export interface Message {
    id?: string,
    text: string,
    user: User,
    isLiked?: boolean,
    editedAt?: string,
    createdAt?: string
}

export interface Chat {
    id: string,
    name: string,
    users: Array<User>,
    messages: Array<Message>
}
