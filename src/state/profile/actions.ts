import {ProfileActionTypes, SET_USER} from './types';
import {User} from '../model-types';

export function setCurrentUser(user: User | undefined): ProfileActionTypes {
    return {
        type: SET_USER,
        payload: user
    }
}
