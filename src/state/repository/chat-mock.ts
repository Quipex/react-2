import {v4 as uuidv4} from 'uuid';
import {mockedMessages, mockedUsers} from './messages-mock';
import {getMessages} from '../../api/service/MessagesService';
import {Chat} from '../model-types';

export async function chatMock(): Promise<Chat> {
    const chatId = uuidv4();
    const chatName = 'My Test chat {' + chatId + '}';
    const msgs = await getMessages();
    const messages = mockedMessages(msgs);
    const users = mockedUsers(messages);
    return {id: chatId, name: chatName, messages, users}
}
