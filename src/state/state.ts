import {ChatState} from './chat/types';
import {ProfileState} from './profile/types';

export default interface State {
    chat: ChatState,
    profile: ProfileState
}
